/*
---------- Details ----------
Name: John McDonnell.
Student Num: D00096987.
Year: 2 Level 8 Computing.
Assignment:	C++ CA 1.
Title: Eurovision Voting Application.
Due Date: 19th October 2014.
Version: Final.
*/



/*
declaring librarys
*/
#include <iostream>
#include <fstream>
#include <string> 
#include <array>
#include <vector>

//global variables
using namespace std;
ofstream outFile;
ifstream thefile;
#define numCountries 5
int countryID;
void voting(vector<string> country);

/*
bubble sort, passing in a reference to the vector country
passing in total votes which the user entered
*/
void bubbleSort(vector<string> &country, int totalVotes[5]) 
{
	int x;
	int j;
	int flag = 1;    // set flag to 1 to start first pass
	int temp = 0;
	string temp2;// holding variable

	while (flag == 1)
	{
		flag = 0;
		for (int i = 0; i < country.size(); i++)
		{
			if (totalVotes[i] < totalVotes[i+1])  // ascending order simply changes to <
			{
				temp = totalVotes[i];       // swap elements
				totalVotes[i] = totalVotes[i + 1];
				totalVotes[i + 1] = temp;

				temp2 = country.at(i);       // swap elements
				country.at(i) = country.at(i + 1);
				country.at(i + 1) = temp2;
				flag = 1;


			}
		}
	}
	
}
/*
bubble sort, passing in the vector country
passing in total votes which the user entered
*/
void excelVotes(vector<string> country, int pointsTable[5][5])
{
	outFile.open("votes.csv"); //opening votes file

	if (!outFile) // checking if the file exists
	{
		cout << "Fail to create open file" << endl;
		exit(EXIT_FAILURE);
	}
	
	outFile << ", ";
	for (int z = 0; z < country.size(); z++)
	{
		outFile << country[z] << ","; //displaying countrys in cell A
	}

	outFile <<endl;
	for (int x = 0; x < country.size(); x++)
	{
		outFile << country.at(x) << ","; //displaying countrys as heading in file
		for (int y = 0; y < country.size(); y++)
		{
			outFile << pointsTable[x][y] << ","; //displaying points that each country earned.
			
		}
		outFile << endl;
	}
	cout << "Finished Writing to file!"<<endl;
	outFile.close(); //closes the excel file.
}
/*
creating voting tables and displaying results
passing in country, total votes and the points table
*/
void votingTable(vector<string> country, int totalVotes[5], int pointsTable[5][5])
{
	for (int u = 0; u < country.size(); u++)
		totalVotes[u] = 0; //setting votes to 0 

	for (int i = 0; i < country.size(); i++)
	{
		for (int j = 0; j < country.size(); j++)
		{
			totalVotes[j] = totalVotes[j] + pointsTable[i][j]; //adding points together
		}
	}
	
	cout << endl;

	bubbleSort(country, totalVotes); //calling bubble sort and retuning countrys
	
	cout << "Voting Table (Ranked)" << endl;
	for (int x = 0; x < country.size(); x++)
	{
		cout << country[x] << "\t" << totalVotes[x] << endl; //printing table with countrys and votes
	}
	cout << endl;
}
/*
getting user input and country index
passing in country and points table
*/
void voting(vector<string> country, int pointsTable[5][5])
{

	int posCountry = -1; 
	int score = 0;
	int selection = 0;
	bool power = true; //bool to control loop

	int x = 0;
	while (power)
	{

		string userInput;
		cout << "Enter name of country voting: " << endl;
		cin >> userInput; // storing userinput

		for (int i = 0; i < country.size(); i++)
		{
			if (userInput.compare(country.at(i)) == 0) //comparing vote at each index to see if it matches user input
			{
				posCountry = i; //place index i into posCountry
			}
		}

			if (posCountry != -1) //selects next country in the file
			{
				cout << "Enter one vote (12, 10, 8 or 6) for each country prompted below: " << endl;

				for (int j = 0; j < country.size(); j++)
				{
					if (posCountry != j)
					{
						bool power2 = true;
						while (power2)
						{
							cout << country.at(j) << " " << endl; //print country at j
							cin >> score; //store county score

							if (selection == score) //stopping the same votes
							{
								cout << "---- Sorry this score has already been assigned to a country ----" << endl;
							}
							else if (score > 12 || score < 6 || score % 2 == 1) //if score > 12 excute this code
							{
								cout << "-Error a vote cannot be greater than 12 and less than 6! -";
								cout << endl;
								power = true;
								power2 = true;
							}
							else
							{
								pointsTable[posCountry][j] = score; // if the above is false excute.	
								power = false;
								power2 = false;
							}
							selection = score; // pass score into selection
						}
					}
				}
			}
			else
			{
				cout << "NOT A COUNTRY! Enter again!" << endl; // if country is not entered
			}
		}
}

int main()
{
	ifstream theFile("countries.dat"); //open the file countries

	int pointsTable[5][5] = {}; //2d array

	int totalVotes[5] = {}; //creating an empty array with 5 spaces
	vector<string> country; //create a vector country

	country.reserve(5); //reserve 5 places in this vector

	string name; 

	cout << "Eurovision Countries:\n" << endl;
	while (getline(theFile, name)) //get each line in the file, and pass them into name
	{
		cout << name << "\n"; // print country name
		country.push_back(name); // pass each country into the vector

	}
	cout << endl;

	bool power = true;
	string on = "";
	while (power){
		voting(country,pointsTable); //calling the voting system
		votingTable(country,totalVotes,pointsTable); //call voting table, pass in country

		cout << "Continue(y/n)?" << endl;
		cin >> on;

		if (on == "n") 
			
			power = false;
	}
	excelVotes(country, pointsTable);
	return 0;
}